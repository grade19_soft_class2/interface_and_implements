﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Show
{
    interface IStudent
    {
        int Id {get; set;}
        string Name {get; set;}
        void Avg();
        void Sum();

    }
}
