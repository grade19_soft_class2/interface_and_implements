﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Show
{
    class Student : IStudent
    {
        public double English { get; set; }

        public double Math { get; set; }

        public double Major { get; set; }

        int IStudent.Id { get; set; }
        string IStudent.Name { get; set; }
        void IStudent.Sum()
        {
          double sum = English + Math + Major;

          Console.WriteLine("总分为:" + sum);
        }
        void IStudent.Avg()
        {
            double avg = (English + Math + Major) / 3;

            Console.WriteLine("平均分为:" + avg);

        }

    }
}
