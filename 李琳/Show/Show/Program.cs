﻿using System;

namespace Show
{
    class Program
    {
        static void Main(string[] args)
        {
            Student a = new Student();

            IStudent student = a ;

            student.Id = 1;

            student.Name = "小红";

            a.English = 96;

            a.Math = 87;

            a.Major = 89;

            Console.WriteLine("学号:" + student.Id);

            Console.WriteLine("姓名:" + student.Name);

            Console.WriteLine("成绩状况如下:");

            student.Sum();

            student.Avg();
        }
    }
}
