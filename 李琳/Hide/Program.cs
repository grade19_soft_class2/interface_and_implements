﻿using System;

namespace Hide
{                                                                                                                               
    class Program
    {
        static void Main(string[] args)
        {
            Student i = new Student();

            IStudent student = i;

            student.Id = 2;

            student.Name = "小明";

            i.English = 90;

            i.Math = 98;

            i.Major = 88;

            //隐式

            Console.WriteLine("学生信息如下:");

            Console.WriteLine("学号：{0}，姓名：{1}", student.Id, student.Name);

            Console.WriteLine("总分为：{0}，平均分为：{1}", student.Avg(), student.Sum());

            Console.ReadKey();


        }

    }
}
