﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hide
{
    interface IStudent
    {
        int Id { get; set; }
        string Name { get; set; }
        double Avg();
        double Sum();



    }
}
