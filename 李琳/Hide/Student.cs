﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hide
{
    class Student : IStudent
    {
        public double English { get; set; }

        public double Math { get; set; }

        public double Major { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }

        
        public double Sum()
        {
            return English + Math + Major;
        }

        public double Avg()
        {
            return (English + Math + Major) / 3;
        }

    }
}
