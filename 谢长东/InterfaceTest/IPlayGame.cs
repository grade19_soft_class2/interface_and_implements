﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceTest
{
    interface IPlayGame
    {

        string GameName { get; set; }
        string GameCompany { get; set; }


        void Play();

    }
}
