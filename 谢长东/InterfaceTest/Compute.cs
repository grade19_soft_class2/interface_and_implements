﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceTest
{
    class Compute:IPlayGame
    {
        public string Brand { get; set; }

        //隐式实现接口
        public string GameName { get ; set ; }
        public string GameCompany { get ; set; }

        public void Play()
        {
            Console.WriteLine("我使用的使{0}电脑，我正在玩{1}游戏，这是一款{2}公司开发的游戏",Brand,GameName,GameCompany);
        }

        


    }


}
