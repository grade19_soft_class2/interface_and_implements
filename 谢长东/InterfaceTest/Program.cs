﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceTest
{
    class Program
    {
        static void Main(string[] args)
        {

            //实现Compute类成员
            Compute com = new Compute();
            com.Brand = "华为";
            com.GameName = "腾讯斗地主";
            com.GameCompany = "腾讯";

            com.Play();




            //实现Phone类成员
            IPlayGame ipg = new Phone();

            (ipg as Phone).Brand = "苹果";
            ipg.GameName = "王者荣耀";
            ipg.GameCompany = "腾讯";

            ipg.Play();



        }
    }
}
