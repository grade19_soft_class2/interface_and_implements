﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceTest
{
    class Phone : IPlayGame
    {


        private string gameName;
        private string gameCompany;

        public string Brand { get; set; }



        //显示实现接口
        string IPlayGame.GameName { get { return gameName; } set { gameName = value; } }
        string IPlayGame.GameCompany { get { return gameCompany; } set { gameCompany = value; } }

        void IPlayGame.Play()
        {
            Console.WriteLine("我使用的使{0}手机，我正在玩{1}游戏，这是一款{2}公司开发的游戏", Brand, gameName, gameCompany);
        }

    }
}
