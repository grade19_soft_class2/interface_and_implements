﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Human : IEat
    {
        void  IEat.IEat()
        {
            Console.WriteLine("人类一败涂地");
        }
    }
    class Man:Human,IEat
    {
         public void IEat()
        {
            Console.WriteLine("人类男人最后的希望，飞向了太空");
        }
    }
    class Woman:Human,IEat
    {
        public void IEat()
        {
            Console.WriteLine("人类女人站了出来，外星人边跑边说“额滴神啊！”");
        }
    }
}
