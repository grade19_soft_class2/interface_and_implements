﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Animals:ISleep
    {
        public  Animals()
        {
            LiuBo = "帅";
        }
        public string LiuBo;
     
        public virtual void AnimalsSleeps()
        {
            Console.WriteLine("动物睡叫");
            
        }

        public virtual void Cats()
        {
            Console.WriteLine("小猫睡觉");
        }
        public virtual void Dogs()
        {
            Console.WriteLine("小狗睡觉");
        }

    }
    class Dog:Animals
    {
        public override void Dogs()
        {
            Console.WriteLine("小猫不睡觉");
        }
        public override void AnimalsSleeps()
        {
            base.AnimalsSleeps();
        }

    }
    class Cat:Animals
    {
        public override void Cats()
        {
            Console.WriteLine("小狗不睡觉");
        }
        public override void AnimalsSleeps()
        {
            base.AnimalsSleeps();
        }
    }
    class Person :Animals
    {
        public Person(string eaxm)
        {
            LiuBo = "liubo大帅比，jianlong超喜欢QAQ";
        }
        public void One()
        {
            Console.WriteLine(base.LiuBo);
        }
        public override void AnimalsSleeps()
        {
            Console.WriteLine("啾咪");
        }
    }
     class Student:Person
    {
        public Student(string eaxm):base(eaxm)
        {
            LiuBo = "just so so";
        }
        public void Two( )
        {
            Console.WriteLine(base.LiuBo);
        }
    }
}
