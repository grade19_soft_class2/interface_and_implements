﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Port
{
    class Baby : IBaby //隐式实现Ibaby接口
    {
        public int Age { get; set; }
        public string Name { get ; set ; }
        public string Sex { get; set ; }

        public void Arr()
        {
            Console.WriteLine("小baby要哭哭惹");
        }

        public void Little()
        {
            Console.WriteLine("小baby{0}太小了，才{1}岁,还是个{2}孩",Name,Age,Sex);
        }
    }
}
