﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Port
{
    class Program
    {
        static void Main(string[] args)
        {
            Baby baby = new Baby();//隐式调用方式直接用类和接口都可以调用
           baby.Name = "馨儿";
            baby.Age = 1; 
            baby.Sex = "女";
            baby.Arr();
            baby.Little();
            Console.ReadLine();

            //显式调用只能接口调用
            IBaby boybaby = new Boy();
            boybaby.Age = 10;
            boybaby.Name = "明明";
            boybaby.Sex = "男";
            Console.WriteLine("{0}孩{1}长大了，他已经{2}岁了",boybaby.Sex,
                boybaby.Name,boybaby.Age);
            boybaby.Arr();
        }
    }
}
