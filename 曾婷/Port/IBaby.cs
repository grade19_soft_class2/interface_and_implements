﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Port
{
    interface IBaby
    {
        int Age { get; set; }
        string Name { get; set; }
        string Sex { get; set; }
        void Arr();
        void Little();

    }
}
