﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Port
{
    class Boy : IBaby //显示实现Ibaby接口
    {
        int IBaby.Age { get ; set ; }
        string IBaby.Name { get ; set ; }
        string IBaby.Sex { get ; set ; }

        void IBaby.Arr() //方法名为接口名.方法名
        {
            Console.WriteLine("这个baby已经可以自己出去玩了");
        }

        void IBaby.Little()
        {
           
        }
    }
}
