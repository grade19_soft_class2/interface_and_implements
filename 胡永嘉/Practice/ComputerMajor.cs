﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
      class ComputerMajor:ICompute
    {
        /* //抽象方式实现接口
          public abstract string Name { set; get; }
         public abstract int ID { set; get; }
         public abstract void Avg();
         public abstract void Total();*/

        //用隐式方式来实现接口成员
        public int ID { set; get; }
        public string Name { set; get; }
        public double English { set; get; }
        public double Programming { set; get; }
        public  double Database { set; get; }
        public void Avg()
        {
            double avg = (English + Programming + Database) / 3;
            Console.WriteLine("平均分："+avg);
        }
        public void Total()
        {
            double total = English + Programming + Database;
            Console.WriteLine("总分："+total);
        }






    }
    //使用显式方式来实现接口成员
   class ComputerMajor2 : ICompute2
    {
        public double English { set; get; }
        public double Programming { set; get; }
        public double Database { set; get; }
        public void Total()
        {
            double total = English + Programming + Database;
            Console.WriteLine("总和："+total);
        }
        int ICompute2.ID { set; get; }
        string ICompute2.Name { set; get; }
        void ICompute2.Avg()
        {
            double avg = (English + Programming + Database) / 3;
            Console.WriteLine("平均分："+avg);
        }
    }






}
