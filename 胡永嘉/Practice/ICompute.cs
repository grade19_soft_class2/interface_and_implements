﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{        //创建一个接口计算学生成绩的接口
        //ICompute,并在接口中分别定义计算
        //总成绩、平均成绩的方法。



  //用隐式方式来实现接口成员
    interface ICompute
    {

        int ID { set; get; }
        string Name { set; get; }
        void Avg();
        void Total();
    }


    //使用显式方式来实现接口成员
    interface ICompute2
    {
        int ID { set; get; }
        string Name { set; get; }
        void Avg();
    }
}
