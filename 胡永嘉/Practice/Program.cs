﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    class Program
    {
        static void Main(string[] args)
        {
            //用隐式方式来实现接口成员
            ComputerMajor computerMajor = new ComputerMajor();
            computerMajor.Name ="小胖";
            Console.WriteLine("姓名："+ computerMajor.Name);
            computerMajor.ID = 1;
            Console.WriteLine("座号："+ computerMajor.ID);
            computerMajor.English = 80;
            computerMajor.Programming = 90;
            computerMajor.Database = 98;
            computerMajor.Avg();
            computerMajor.Total();

            //使用显式方式来实现接口成员
            ComputerMajor2 computerMajor2 = new ComputerMajor2();
            ICompute2 compute2 = computerMajor2;
            compute2.Name = "橙子";
            Console.WriteLine("名字："+ compute2.Name);
            compute2.ID = 2;
            Console.WriteLine("桌号："+ compute2.ID);


            computerMajor2.English = 80;
            computerMajor2.Database = 90;
            computerMajor2.Programming = 95;

            computerMajor2.Total();
            compute2.Avg();
        }
    }
}
