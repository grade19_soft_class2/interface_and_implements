﻿using System;

namespace InterfaceTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Dog dog = new Dog();
            dog.cry();
            dog.run();
            IAnimal animal = new Panda();
            animal.run();
            animal.cry();




            //隐式调用。
            BookList bklist = new BookList();
            bklist.Add("隐式");
            Console.Write("实现隐式接口的例子:使用类：{0}", bklist[0]);
            IBookList ibklist = (IBookList)bklist;    
            ibklist.Add("隐式");
            Console.Write("实现隐式接口的例子：使用接口{0}", ibklist[1]);
            //显示调用   只能用接口调用。
            BookList2 bklist2 = new BookList2();            
            IBookList Ibk = (IBookList)bklist2;
            Ibk.Add("显示");
           Console.Write("实现显示接口的例子:使用接口{0} ", Ibk[0]);
            Console.ReadLine();
        }
    }
}
