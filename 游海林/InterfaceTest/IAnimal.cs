﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceTest
{
    interface IAnimal
    {
        void cry();
        void run();

    }
    class Dog : IAnimal
    {
        public void cry()
        {
            Console.WriteLine("我会叫哦！");
        }

        public void run()
        {
            Console.WriteLine("我会跑哦！");
        }
    }
    class Panda : IAnimal
    {
        void IAnimal.cry()
        {
            Console.WriteLine("我叫的声音很好听哦！");
        }

        void IAnimal.run()
        {
            Console.WriteLine("我跑的很快哦，你可能追不上哦！");
        }
    }
}
