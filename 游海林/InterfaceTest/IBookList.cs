﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceTest
{
   public interface IBookList
    {
        void Add(string BookName);
        void Append(string BookName);
        void Remove(int position);
        int Count { get; }
        string this[int index] { get; set; }
    }
    //隐式调用
    public class BookList : IBookList
    {
        private List<string> booklist = new List<string>();
        #region IBookList 成员
        public void Add(string BookName)
        {
            booklist.Add(BookName);
        }
        public void Append(string BookName)
        {
            booklist.Insert(booklist.Count, BookName);
        }
        public void Remove(int position)
        {
            booklist.RemoveAt(position);
        }
        public int Count
        {
            get
            {
                return booklist.Count;
            }
        }
        public string this[int index]
        {
            get
            {
                return booklist[index];
            }
            set
            {
                booklist[index] = value;
            }
        }
        #endregion
    }
    //显示调用    
    public class BookList2 : IBookList
    {
        private List<string> booklist = new List<string>();
        #region IBookList 成员
        void IBookList.Add(string BookName)
        {
            booklist.Add(BookName);
        }
        void IBookList.Append(string BookName)
        {
            booklist.Insert(booklist.Count, BookName);
        }
        void IBookList.Remove(int position)
        {
            booklist.Remove(position.ToString());
        }
        int IBookList.Count
        {
            get
            {
                return booklist.Count;
            }
        }
        string IBookList.this[int index]
        {
            get
            {
                return booklist[index];
            }
            set
            {
                booklist[index] = value;
            }
        }
        #endregion
    }
}
