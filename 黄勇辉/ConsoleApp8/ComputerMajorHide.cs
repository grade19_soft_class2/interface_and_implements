﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShowHidden
{
    interface IComepute
    {
        int ID { get; set; }
        string Name { get; set; }
        void Total();
        void Avg();
    }
    class ComputerMajorHide : IComepute
    {
        public int ID { get; set; }              //隐式显示接口中的属性
        public string Name { get; set; }         //隐式显示接口中的属性
        public double English { get; set; }      //隐式显示接口中的属性
        public double Programming { get; set; }  //隐式显示接口中的属性
        public double Database { get; set; }     //隐式显示接口中的属性
        public void Score()
        {
            Console.WriteLine("English:" + English);
            Console.WriteLine("Programming:" + Programming);
            Console.WriteLine("Database:" + Database);
        }
        public void Avg()   //隐式实现接口中的方法
        {
            double avg = (English + Programming + Database) / 3;
            Console.WriteLine("平均分:" + Math.Round(avg, 2));
        }
        public void Total() //隐式实现接口中的方法
        {
            double sum = English + Programming + Database;
            Console.WriteLine("总分：" + sum);
        }
    }
}