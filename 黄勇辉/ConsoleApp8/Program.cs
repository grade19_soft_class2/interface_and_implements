﻿using ShowHidden;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            // 隐式实现接口
            ComputerMajorHide computerMajorHide = new ComputerMajorHide();
            computerMajorHide.ID = 1;
            computerMajorHide.Name = "小明";
            computerMajorHide.Programming = 99;
            computerMajorHide.English = 134;
            computerMajorHide.Database = 121;
            Console.WriteLine("学号:" + computerMajorHide.ID);
            Console.WriteLine("姓名:" + computerMajorHide.Name);
            Console.WriteLine("成绩信息如下：");
            computerMajorHide.Score();
            computerMajorHide.Avg();
            computerMajorHide.Total();
            Console.WriteLine("——————");





            //显示实现接口
            ComputerMajorExplicit computerMajorExplicit = new ComputerMajorExplicit();
            IComeputer comeputer = computerMajorExplicit;
            comeputer.ID = 2;
            comeputer.Name = "小可";
            computerMajorExplicit.English = 101;
            computerMajorExplicit.Programming = 120;
            computerMajorExplicit.Database = 110;
            Console.WriteLine("学号:" + comeputer.ID);
            Console.WriteLine("姓名:" + comeputer.Name);
            Console.WriteLine("成绩信息如下：");
            computerMajorExplicit.Score();
            computerMajorExplicit.Total();
            computerMajorExplicit.Avg();
        }
    }
}
