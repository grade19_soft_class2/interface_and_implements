﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp15
{
    class Message2:Imessage 
    {
        public double LifeValue { get; set; }
        public double PhysicalStrength { get; set; }
      
        int Imessage.Id { get; set; }           //显示实现接口中的属性
       string  Imessage.Name { get; set; }      //显示实现接口中的属性
        void Imessage.Total()                   //显示实现接口中的方法
        {
            double sum = LifeValue + PhysicalStrength;
            Console.WriteLine("总分数：" + sum);
        }
        void Imessage.Avg()
        {
            double avg = (LifeValue + PhysicalStrength) / 2;
            Console.WriteLine("平均分为：" + avg);
        }
    }
}
