﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp15
{
    class Message:Imessage
    {
        
    public int Id { get; set; }    
        public string Name { get; set; }    
        public double LifeValue { get; set; }
        public double PhysicalStrength { get; set; }
    
        public void Avg()     
        {
            double avg = (LifeValue + PhysicalStrength) / 2;
            Console.WriteLine("平均分：" + avg);
        }
        public void Total()
        {
            double sum = LifeValue + PhysicalStrength;
            Console.WriteLine("总分为：" + sum);
        }
    }
}
