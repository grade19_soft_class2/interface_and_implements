﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp15
{
    interface Imessage
    {
        int Id { get; set; }
        string Name { get; set; }
        void Total();
        void Avg();
    }
}
