﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class ComputerMajor : ICompute
    {
        public int Id { get; set; }     //隐式的实现接口中的属性
        public string Name { get; set; }    //隐式实现接口中的属性
        public double chinese { get; set; }
        public double math { get; set; }
        public double PE { get; set; }
        public void Avg()       //隐式实现接口中的方法
        {
            double avg = (chinese  + math  + PE ) / 3;
            Console.WriteLine("平均分：" + avg);
        }
        public void Total()
        {
            double sum = chinese  + math  + PE ;
            Console.WriteLine("总分为：" + sum);
        }



        public double English { get; set; }
        public double Programming { get; set; }
        public double Database { get; set; }
        int ICompute.Id { get; set; }           //显示实现接口中的属性
        string ICompute.Name { get; set; }      //显示实现接口中的属性
        void ICompute.Total()                   //显示实现接口中的方法
        {
            double sum = English + Programming + Database;
            Console.WriteLine("总分数：" + sum);
        }
        void ICompute.Avg()
        {
            double avg = (English + Programming + Database) / 3;
            Console.WriteLine("平均分为：" + avg);
        }
    }



}
