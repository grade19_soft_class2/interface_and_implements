﻿namespace ConsoleApp1
{
    internal interface ICompute
    {
        int Id { get; set; }
        string Name { get; set; }

        void Total();
        void Avg();
    }
}