﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceTest
{
    interface ICompute
    {
        int Id { get; set; }
        string Name { get; set; }
        void Total();
        void Avg();
    }
}
