﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceTest
{
       //隐式
           class ComputerMajor : ICompute
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public double English { get; set; }
            public double Programming { get; set; }
            public double Datebase { get; set; }
            public void Avg()
            {
                double avg = (English + Programming + Datebase) / 3;
                Console.WriteLine("平均分为:" + avg);
            }
            public void Total()
            {
                double sum = (English + Programming + Datebase);
                Console.WriteLine("总分为:" + sum);
            }
        }
}
