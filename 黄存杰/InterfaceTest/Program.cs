﻿using System;

namespace InterfaceTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //隐式
            ComputerMajor ComputerMajor = new ComputerMajor();
            ComputerMajor.Id = 1;
            ComputerMajor.Name = "李明";
            ComputerMajor.English = 80;
            ComputerMajor.Programming = 90;
            ComputerMajor.Datebase = 85;
            Console.WriteLine("学号:{0} 姓名:{1}", ComputerMajor.Id, ComputerMajor.Name);
            Console.WriteLine("成绩如下");
            ComputerMajor.Avg();
            ComputerMajor.Total();


            Console.WriteLine();

            //显示
            ComputerMjor2 computerMjor2 = new ComputerMjor2();
            ICompute compute = computerMjor2;
            compute.Id = 1;
            compute.Name = "李明";
            computerMjor2.English = 80;
            computerMjor2.Programming = 90;
            computerMjor2.Datebase = 85;
            Console.WriteLine(computerMjor2.English);
            Console.WriteLine("学号:{0} 姓名:{1}", compute.Id, compute.Name);
            Console.WriteLine("成绩如下");
            compute.Avg();
            compute.Total();
        }
    }
}
