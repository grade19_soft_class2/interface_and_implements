﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceTest
{

    //显示
    class ComputerMjor2:ICompute
    {
        public double English { get; set; }
        public double Programming { get; set; }
        public double Datebase { get; set; }
        int ICompute.Id { get; set; }
        string ICompute.Name { get; set; }
        void ICompute.Avg()
        {
            double avg = (English + Programming + Datebase) / 3;

            Console.WriteLine("平均分为:" + avg);
        }
        void ICompute.Total()
        {
            double sum = (English + Programming + Datebase);
            Console.WriteLine("总分为:" + sum);
        }
    }
}
