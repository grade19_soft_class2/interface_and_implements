﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Display
{
    class Program
    {
        static void Main(string[] args)
        {
            //隐式接口实现
            Test test = new Test();
            test.Name = " 小李子";
            test.Sex = "男";
            test.Age = 20;
            test.X = 120.5;
            test.Y = 95;
            test.Z = 99;
            Console.WriteLine( "此处为隐式接口");
            Console.WriteLine( "姓名"+test.Name);
            Console.WriteLine( "性别"+test.Sex);
            Console.WriteLine( "年龄"+test.Age);
            Console.WriteLine( "三个数的和"+test.Tatol());
            Console.WriteLine( "平均数"+test.Avg());

        }
    }
}
