﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implicit
{
    class Program
    {
        static void Main(string[] args)
        {
            //显式接口实现
            Test test = new Test();
            ITest ts = test;//实例化接口
            //赋值
            ts.Age = 20;
            ts.Id = 9;
            ts.Major = "软件技术";
            ts.Name = "小李子";
            ts.Sex = "男";
            test.A = 100;    
            test.B = 100;    
            test.C = 100;
            //输出
            Console.WriteLine("这里为显式接口实现");
            Console.WriteLine("基本信息： 姓名{0}, 年龄{1}, 性别{2}", ts.Name ,ts.Age,ts.Sex);
            Console.WriteLine("专业为{0},学号{1}",ts.Major,ts.Id);
            Console.WriteLine("三数和{0}：,平均数{1}",ts.Tatol(),ts.Avg());
           
            
        }
    }
}
