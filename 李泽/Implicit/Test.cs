﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implicit
{
    class Test : ITest
    {
        string ITest.Name { set; get; }
        string ITest.Major { set; get; }
        int ITest.Id { set; get; }
        int ITest.Age { set; get; }
        string ITest.Sex { set; get; }
        public int A { set; get; }
        public int B { set; get; }
        public int C { set; get; }
       int ITest.Avg()
        {
            return (A + B + C) / 3;
        }
        int ITest.Tatol()
        {
            return A + B + C;
        }
    }
}
