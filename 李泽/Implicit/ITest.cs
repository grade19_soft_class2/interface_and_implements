﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implicit
{
    interface ITest
    {
        string Name { set; get; }
        string Sex{ set; get; }
        string Major { set; get; }
      int Id { set; get; }
       int Age { set; get; }
        int Avg();
        int Tatol();
    }
}
