﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
         ComputerMajor computerMajor = new ComputerMajor();
            computerMajor.Id = 1;
            computerMajor.Name = "许梦婷";
            computerMajor.English = 94;
            computerMajor.Programming = 87;
            computerMajor.Database = 89;
            Console.WriteLine("学号：" + computerMajor.Id);
            Console.WriteLine("姓名：" + computerMajor.Name);
            Console.WriteLine("成绩信息如下：");
            computerMajor.Total();
            computerMajor.Avg();
          
        }
    }
}


