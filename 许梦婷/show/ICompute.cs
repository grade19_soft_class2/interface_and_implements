﻿using System;
using System.Collections.Generic;
using System.Text;

namespace show
{
    interface ICompute
    {
        int Id { get; set; }
        string Name { get; set; }
        void  Total();
        void Avg();
    }
}
