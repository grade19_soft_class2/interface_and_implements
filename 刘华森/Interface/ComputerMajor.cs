﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    //显示接口
    class ComputerMajor : ICompute
    {
        /*  public double English { get; set; }
          public double Programing { get; set; }
          public double Databse { get; set; }
          int ICompute.Id { get; set; }//显示实现接口中的属性
          string ICompute.Name { get; set; }//显示实现接口中的属性
          void ICompute.Total()//显示实现接口中的方法
          {
              double sum = English + Programing + Databse;
              Console.WriteLine("总分数："+sum );
          }
          void ICompute.Avg()
          {
              double avg = (English + Programing + Databse) / 3;
              Console.WriteLine("平均分："+avg );*/

        //隐式接口

        public int Id { get; set; }//隐式的实现接口中的属性
        public string Name { get; set; }//隐式实现接口中的属性
        public double English { get; set; }
        public double Programmig { get; set; }
        public double Database { get; set; }
        public void Avg()//实现接口中的方法
        {
            double avg = (English + Programmig + Database)/3;
            Console.WriteLine("平均分为："+avg  );
        }

        public void Total()
        {
            double sum = English + Programmig + Database;
            Console.WriteLine("总分为："+sum );
        }
    }
}


