﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
     interface ITest
    {
        void methodA();
    }
    class Test1:ITest
    {
        public void methodA()
        {
            Console.WriteLine("Test1类中的methodA方法");
        }
    }
    class Test2 : ITest
    {
        public void methodA()
        {
            Console.WriteLine("Test2类中的methodA方法");
        }
    }
}
