﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    class Program
    {
        static void Main(string[] args)
        {
            //隐式接口
            ComputerMajor computerMajor = new ComputerMajor();
            computerMajor.Id = 1;
            computerMajor.Name = "小李";
            computerMajor.English = 80;
            computerMajor.Programmig = 90;
            computerMajor.Database = 84;


            Console.WriteLine("学号："+computerMajor.Id);
            Console.WriteLine("姓名："+computerMajor.Name);
            Console.WriteLine("成绩如下：");
            computerMajor.Total();
            computerMajor.Avg();

            //显示接口
            /* ComputerMajor computerMajor = new ComputerMajor();
             ICompute compute = computerMajor;//创建接口的实例
             compute.Id = 1;
             compute.Name = "小明";
             computerMajor.English = 80;
             computerMajor.Programing = 90;
             computerMajor.Databse = 84;

             Console.WriteLine("学号："+compute.Id);
             Console.WriteLine("姓名："+compute.Name);
             Console.WriteLine("成绩信息如下：");
             compute.Total();
             compute.Avg();*/

            // C#接口中多态的实现
            ITest test1 = new Test1();//创建接口的实例test1指向实例Test1的对象
            test1.methodA();
            ITest test2 = new Test2();//创建接口的实例test2指向实现类Test2的对象
            test2.methodA();

            Console.WriteLine();

            //C#接口中的多态的实现

            IShape shape1 = new Rectangle(10, 90);
            shape1.X = 100;
            shape1.Y = 200;
            shape1.Color = "绿色";
            shape1.Draw();
            Console.WriteLine();
            IShape shape2 = new Circle(10);
            shape2.X = 300;
            shape2.Y = 500;
            shape2.Color = "蓝色";
            shape2.Draw();
        }
    }
}
