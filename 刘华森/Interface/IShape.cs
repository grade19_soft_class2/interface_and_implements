﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    interface IShape
    {
        double Area { get; }
        double X { get; set; }
        double Y { get; set; }
        string Color { get; set; }
        void Draw();
    }
}
