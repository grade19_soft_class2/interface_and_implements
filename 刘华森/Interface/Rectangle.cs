﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    class Rectangle : IShape
    {
        //public double Area => throw new NotImplementedException();

        public double X { get; set; }
        public double Y { get ; set ; }
        public string Color { get ; set; }
        public double Length { get; set; }//定义长方形的长度
        public double Width { get; set; }//定义长方形的宽度
        public double Area
        {
            get
            {
                return Length * Width;//计算长方形面积
            }
        }

        public Rectangle(double length,double width)
        {
            this.Length= length;
            this.Width = width;
        }
        
        public void Draw()
        {
            //throw new NotImplementedException();

            Console.WriteLine("绘制图形如下：");
            Console.WriteLine("在坐标{0}，{1}的位置绘制面积为{2}颜色为{3}的矩形",X,Y,Area,Color);
        }
    }
    class Circle : IShape
    {
        //为圆的半径赋值
       // public double Area => throw new NotImplementedException();
        public double Area
        {
            get
            {
                return Radius * Radius * 3.14;
            }
        }
        public double Radius { get; set; }
        public Circle(double radius)
        {
            this.Radius = radius;
        }
        public double X { get ; set ; }
        public double Y { get ; set; }
        public string Color { get ; set ; }

        public void Draw()
        {
            Console.WriteLine("绘制图形如下：");
            Console.WriteLine("在坐标为{0}，{1}的位置绘制面积为{2}颜色为{3}的圆形",X,Y,Area,Color);
        }
    }
}
