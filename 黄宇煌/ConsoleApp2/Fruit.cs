﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class Fruit : IFruit
    {
        void IFruit.Fruit()
        {
            Console.WriteLine("请你吃水果");
        }
    }

    class Apple : IFruit
    {
        public void Fruit()
        {
            Console.WriteLine("苹果你要不要");
        }
    }

    class Banana : IFruit
    {
        public void Fruit()
        {
            Console.WriteLine("那就只有香蕉了");
        }
    }
}
