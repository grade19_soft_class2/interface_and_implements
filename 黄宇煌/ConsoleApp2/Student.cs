﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    //抽象类调用部分接口成员
     abstract class Report : IReport
    {
        public abstract int Id { get; set; }
        public abstract string Name { get; set; }

        public void Sum()
        {
            Console.WriteLine("抽象类调用部分成员");
        }
    }
    //隐式实现接口
    class Student : IReport
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int English { get; set; }
        public int Math { get; set; }
        public int Chinese { get; set; }
        public void Sum()
        {
            int sum = English + Math + Chinese;
            Console.WriteLine("总成绩:" + sum);
        }

    }
    //显式实现接口
    class Studengt1 : IReport
    {
        public int English { get; set; }
        public int Math { get; set; }
        public int Chinese { get; set; }
        int IReport.Id { get; set; }
        string IReport.Name { get; set; }
        
        void IReport.Sum()
        {
            int sum = English + Math + Chinese;
            Console.WriteLine("总分:" + sum); 
        }
    }

}
