﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    interface IReport
    {
        int Id { get; set; }
        string Name { get; set; }
        void Sum();
    }
}
