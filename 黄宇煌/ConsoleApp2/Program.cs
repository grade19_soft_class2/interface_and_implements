﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //隐式接口调用
            Student student = new Student();
            student.Id = 1;
            student.Name = "李白";
            student.English = 10;
            student.Math = 84;
            student.Chinese = 87;
            Console.WriteLine("学号："+student.Id);
            Console.WriteLine("姓名："+student.Name);
            student.Sum();

            //显式接口调用
            Studengt1 studengt1 = new Studengt1();
            IReport s = studengt1;
            s.Id = 2;
            s.Name = "李世民";
            studengt1.English = 43;
            studengt1.Math = 80;
            studengt1.Chinese = 79;
            Console.WriteLine("学号：" + s.Id);
            Console.WriteLine("姓名：" + s.Name);
            s.Sum();

            //多态接口调用
            Fruit fruit = new Fruit();
            IFruit f = fruit;
            f.Fruit();

            Apple apple = new Apple();
            apple.Fruit();

            Banana banana = new Banana();
            banana.Fruit();
        }
    }
}
