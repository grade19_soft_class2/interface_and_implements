﻿using System;

namespace ConsoleApp18
{
    class Program
    {
        static void Main(string[] args)
        {
            Message message = new Message();
            message.Id = 5;
            message.Name = "hehe";
            message.LifeValue = 22;
            message.PhysicalStrength = 33;
            Console.WriteLine("编号：" + message.Id);
            Console.WriteLine("名字：" + message.Name);
            Console.WriteLine("成绩");
            message.Total();
            message.Avg();


            Message2 message1 = new Message2();
            Imessage imessage = message1;
            imessage.Id = 5;
            imessage.Name = "xiao";
            message1.LifeValue = 34;
            message1.PhysicalStrength = 43;

            Console.WriteLine("学号：" + imessage.Id);
            Console.WriteLine("姓名:" + imessage.Name);
            Console.WriteLine("成绩信息如下：");
            imessage.Total();
            imessage.Avg();

        }
    }

    
}
