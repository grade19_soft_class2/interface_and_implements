﻿using System;

namespace Computer
{
        //定义一个接口
        interface IComputer
        {
            void Sleep();
            void Drink();

        }
        //实现接口
        class Program01
        {
        }
        class Person : Program01, IComputer
        {
            public string Name { get; set; }
            public int Age { get; set; }

            public void Sleep()
            {
                Console.WriteLine("她在睡觉");
            }
            public void Drink()
            {
                Console.WriteLine("她在喝茶");
            }


        
    }
}
