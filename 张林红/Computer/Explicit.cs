﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace Computer02
{
    //显式接口的实现
    interface IComputer02
    {
        public string NickName { get; set; }
        public int NickAge { get; set; }
        public void _Family();
        public void _Iocal();
    }
    class Explicit:IComputer02
    {
        //显示实现接口中的属性
        int IComputer02.NickAge { get; set; }
        string IComputer02.NickName { get; set; }
        //显示实现接口中的方法
        void IComputer02._Family()
        {
            
            Console.WriteLine(3);
        }
        void IComputer02._Iocal()
        {
            
            Console.WriteLine("浙江");
        }
        
    }
}
