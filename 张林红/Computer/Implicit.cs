﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Computer01
{
    interface IComputer01
    {
        string Name { get; set; }
        int Age { get; set; }
        void Family();
        void Iocal();
    }

    class Implicit : IComputer01
    {
        //隐式的实现接口
        public int Age { get; set; }
        public string Name { get; set; }
        public string _Iocal { get; set; }
        public int _Family { get; set; }
        //隐式接口实现方法
        public void Iocal()
        {
            string Iocal = _Iocal;
            Console.WriteLine(Iocal);
        }

        public void Family()
        {
            int Family = _Family;
            Console.WriteLine(Family);
        }
    }
}
