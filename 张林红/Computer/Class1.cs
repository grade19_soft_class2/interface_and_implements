﻿using Computer01;
using Computer02;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace Computer
{
    class Class1
    {
        static void Main(string[] args)
        {
            Person pr = new Person();
            pr.Drink();
            pr.Sleep();

            //隐式
            Implicit im = new Implicit();
            im.Age = 19;
            im.Name = "Alice";
            im._Family = 4;
            im._Iocal = "福建";
            Console.WriteLine("名字是" + im.Name);
            Console.WriteLine("年龄是" + im.Age);
            Console.WriteLine("她家的人口数是");
            im.Family();
            Console.WriteLine("她生活的地方是：");
            im.Iocal();

            //显式
            Explicit ex = new Explicit();
            IComputer02 ic = ex;
            ic.NickAge = 20;
            ic.NickName = "Bob";
            Console.WriteLine("名字是" + ic.NickName);
            Console.WriteLine("年龄是" + ic.NickAge);
            Console.WriteLine("他家的人口数是");
            ic._Family();
            Console.WriteLine("他生活的地方是：");
            ic._Iocal();

        }
    }
}
