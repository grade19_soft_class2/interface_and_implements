﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    class Program
    {
        static void Main(string[] args)
        {
            Speaker s = new Speaker();
            Console.WriteLine(s.Behavior());

            IAmericans a = new American();
            Console.WriteLine(a.Speak());

         }
    }
}
