﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    public interface  IChinese
    {
        string Behavior();
    }

    public class Speaker:IChinese
    {
        public string Behavior()
        {
            return "中文";
        }
    }


}
