﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    public interface  IAmericans
    {
        string Speak();
    }

    public class American:IAmericans
    {
        string IAmericans.Speak()
        {
            return "English";
        }
    }
}
