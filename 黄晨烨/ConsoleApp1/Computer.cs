﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Computer : IComputer
    {/// <summary>
    /// 显式方式实现接口
    /// </summary>
        int IComputer.ComputerId { get; set ; }
        string IComputer.ComputerBrand { get ; set ; }
        string IComputer.ComputerColor { get ; set ; }
        int IComputer.ComputerPrice { get ; set; }
        int IComputer.Container { get ; set ; }

        
    }
   
    
    }
}
