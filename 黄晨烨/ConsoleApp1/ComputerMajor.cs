﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ConsoleApp1
{
    class ComputerMajor : ICompute
    {/// <summary>
    /// 隐式方式实现接口
    /// </summary>
        public int Id { get ; set; }
        public string Name { get ; set ; }
        public int English { get ; set; }
        public int Gramming { get; set ; }
        public int Datebase { get ; set ; }

        public void Avg()
        {
            double Avg = (English + Gramming + Datebase) / 3;
            Console.WriteLine("平均成绩："+Avg);
        }

        public void Sum()
        {
            double Sum = English + Gramming + Datebase;
            Console.WriteLine("总分为："+Sum);
        }
        public void All()
        {
            Console.WriteLine("学号:{0}  姓名：{1}  英语成绩：{2}  编程成绩：{3}   数据库成绩：{4}",this.Id,this.Name,this.English,this.Gramming,this.Datebase);
        
        }
    }
}
