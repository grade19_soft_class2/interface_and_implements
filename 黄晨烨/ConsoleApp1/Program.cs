﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            ComputerMajor computerMajor = new ComputerMajor();
            computerMajor.Id = 1;
            computerMajor.Name = "胡图图";
            computerMajor.English = 98;
            computerMajor.Gramming = 99;
            computerMajor.Datebase = 89;
            computerMajor.All();
            computerMajor.Avg();
            computerMajor.Sum();


            Computer computer = new Computer();
            IComputer equipment = computer;//在调用显式接口的时候，需用接口的实例来调用
            equipment.ComputerId = 2346532;
            equipment.ComputerBrand = "联想";
            equipment.ComputerColor = "银色";
            equipment.ComputerPrice = 4999;
            Console.WriteLine("型号："+equipment.ComputerId);
            Console.WriteLine("品牌：" + equipment.ComputerBrand);
            Console.WriteLine("颜色：" + equipment.ComputerColor);
            Console.WriteLine("价格：" + equipment.ComputerPrice);

        }
    }
}
