﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    interface ICompute
    {
        int Id { get;set; }
        string Name { get; set; }
        int English { get; set; }
        int Gramming { get; set; }
        int Datebase { get; set; }
        void Avg();
        void Sum();
    }
}
