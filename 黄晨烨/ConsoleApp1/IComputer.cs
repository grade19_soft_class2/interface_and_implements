﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    interface IComputer
    {
        int ComputerId { get; set; }
        string ComputerBrand { get; set; }
        string ComputerColor { get; set; }
        int ComputerPrice { get; set; }
        int Container { get; set; }
       
    }
}
