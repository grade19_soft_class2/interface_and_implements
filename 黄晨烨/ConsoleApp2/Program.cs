﻿using System;
using System.ComponentModel;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            ///多态
            
            Cat cat = new Cat();
            cat.ShowInfo();
            Dog dog = new Dog();
            dog.ShowInfo();

            IAnimal animal = new Dog();
            animal.ShowInfo();
            IAnimal animal1 = new Cat();
            animal1.ShowInfo();


        }
    }
}
