﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    interface IAnimal
    {
        int Id {get; set;}

        string Name {get;set;}

        void Eat();
    }
 
}
