﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal animal = new Animal();
            animal.Id = 5;
            animal.Name = "兔子";
            Console.WriteLine("动物的编号： "+ animal.Id);
            Console.WriteLine("动物名称："+ animal.Name);
            animal.Eat();

            Console.WriteLine();
                
            Dog dog = new Dog();
            dog.Name = "狗";
            dog.Eat();

            Console.WriteLine();

            Class1 class1 = new Class1();
            IAnimal animal1 = class1;  //创建接口的实例
            animal1.Id = 4;
            animal1.Name = "乌龟";
            Console.WriteLine("动物的编号： "+ animal1.Id);
            Console.WriteLine("动物名称："+ animal1.Name);
            animal1.Eat();

            Console.WriteLine();

            Cat cat = new Cat();
            cat.Eat();
           
        }
    }
}
