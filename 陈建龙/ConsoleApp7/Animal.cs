﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    public class Animal : IAnimal
    {
        public int Id { get; set; } //隐式的实现接口中的属性
        public string Name { get; set; }

        public void Eat()   //隐式实现接口中的方法
        {
            Console.WriteLine("编号为"+Id+"的"+Name+"吃东西");
        }
      }
         public class Dog: Animal
        {
            public void Eat()
            {
                Console.WriteLine(Name+"吃凉皮");
            }
         }

}
