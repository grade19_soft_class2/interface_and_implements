﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    //接口
    interface IComeputer
    {
        int Id { get; set; }
        string Name { get; set; }
        void Total();
        void Avg();
    }
    class ComputerMajorExplicit : IComeputer
    {
        public double English { get; set; }
        public double Programming { get; set; }
        public double Database { get; set; }
        int IComeputer.Id { get; set; }//显示实现接口中的属性
        string IComeputer.Name { get; set; }//显示实现接口中的属性
        public void Score()//显示实现接口中的方法
        {
            Console.WriteLine("English:" + English);
            Console.WriteLine("Programming:" + Programming);
            Console.WriteLine("Database:" + Database);
        }
        public void Avg() //显式实现接口中的方法
        {
            double avg = (English + Programming + Database) / 3;
            Console.WriteLine("平均分：" + avg);
        }
        public void Total()    //显式实现接口中的方法
        {
            double sum = English + Programming + Database;
            Console.WriteLine("总分为：" + sum);
        }
    }
}
