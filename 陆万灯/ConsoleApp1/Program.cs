﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //隐式实现接口
            ComputerMajorHide computerMajorHide = new ComputerMajorHide();
            computerMajorHide.Id = 1;
            computerMajorHide.Name = "李狗蛋";
            computerMajorHide.Programming = 86;
            computerMajorHide.English = 89;
            computerMajorHide.Database = 92;
            Console.WriteLine("学号:" + computerMajorHide.Id);
            Console.WriteLine("姓名:" + computerMajorHide.Name);
            Console.WriteLine("成绩如下");
            computerMajorHide.Score();
            computerMajorHide.Avg();
            computerMajorHide.Total();

            Console.WriteLine();

            //显式实现接口
            ComputerMajorExplicit computerMajorExplicit = new ComputerMajorExplicit();
            IComeputer comeputer = computerMajorExplicit;
            comeputer.Id = 2;
            comeputer.Name = "王二麻子";
            computerMajorExplicit.English = 85;
            computerMajorExplicit.Programming = 73;
            computerMajorExplicit.Database = 64;
            Console.WriteLine("学号：" + comeputer.Id);
            Console.WriteLine("姓名：" + comeputer.Name);
            Console.WriteLine("成绩信息如下：");
            computerMajorExplicit.Score();
            computerMajorExplicit.Total();
            computerMajorExplicit.Avg();
        }
    }
}
