﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Person : IText   //隐示接口
    {
        public void Fell()
        {
            throw new NotImplementedException();
        }

        public void Reas()
        {
            Console.WriteLine("今天你那里天气怎么样");
        }
    }

    class Have : IText //显示接口
    {
        void IText.Fell()
        {
            Console.WriteLine("还好吧，阴天"); 

        }

        void IText.Reas()
        {
            Console.WriteLine("是吗"); 
        }
    }
}
