﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{

    //显示实现接口

    //public class computer:IComputer 
    //{   

    //   public  double English { get; set; }
    //   public  double datebase { get; set; }
    //   public  double Program { get; set; }
    //    int IComputer.id { get; set; }      //显示实现接口属性
    //    string IComputer.name { get; set; } //显示实现接口属性
    //    void IComputer.Avg()                //显示实现接口方法
    //    {
    //        double avg = (English + datebase + Program) / 3;
    //        Console.WriteLine("平均分"+avg );
    //    }
    //    void IComputer .Total()             //显示实现接口方法
    //    {
    //        double sum = English + datebase + Program;
    //        Console.WriteLine("总和"+ sum );
    //    }
    //}



    //隐式实现接口

    public class computer : IComputer
    {   

        public double English { get; set; }
        public double datebase { get; set; }
        public double Program { get; set; }
        public int id { get; set; }     //隐式实现接口中的属性
        public string name { get; set; }//隐式实现接口中的属性
        public void Avg()               //隐式实现接口中的方法
        {
            double avg = (English + datebase + Program) / 3;
            Console.WriteLine("平均分"+avg );
        }
        public void Total()              //隐式实现接口中的属性
        {
            double sum = English + datebase + Program;
            Console.WriteLine("总和"+sum);
        }
    }
}
