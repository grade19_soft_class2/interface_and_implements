﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    interface IComputer
    {
        int id  { get; set; }
        string name  { get; set; }
        void Avg();
        void Total();

    }
}
