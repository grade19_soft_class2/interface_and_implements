﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work
{
    class Program
    {
        static void Main(string[] args)
        {
            IWork work1 = new Work1(); //创建接口的实例work1指向实现类Work1的对象
            IWork work2 = new Work2(); //创建接口的实例work2指向实现类Work2的对象
            work1.PrintInfo();
            work2.PrintInfo();
        }
    }
}
