﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work
{
    interface IWork
    {
        void PrintInfo();
    }
    class Work1 : IWork  //接口下的多态
    {
        public void PrintInfo()
        {
            Console.WriteLine("Work1类中的PrintInfo方法");
        }
    }
    class Work2 : IWork
    {
        public void PrintInfo()
        {
            Console.WriteLine("Work2类中的PrintInfo方法");
        }
    }
}
