﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text
{
    class Program
    {
        static void Main(string[] args)
        {
            ComputerMajor major = new ComputerMajor();
            major.Id = 6;
            major.Name = "黄炎榕";
            major.Sex = "男";
            major.X = 120;
            major.Y = 140;
            major.Z = 278;
            Console.WriteLine("此处为隐式接口：");
            Console.WriteLine("学生信息：");
            Console.WriteLine("学号：{0}，姓名：{1}，性别：{2}", major.Id, major.Name, major.Sex);
            Console.WriteLine("成绩信息：");
            Console.WriteLine("总分为：{0}，平均分为：{1}", major.Total(), major.Avg());
            Console.ReadKey();
        }
    }
}
