﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text
{
    class ComputerMajor : IComputer
    {
        public int Id { get; set; } //隐式实现接口中的属性
        public string Name { get; set; }
        public string Sex { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        
        public double Total() //隐式实现接口中的方法
        {
            return X + Y + Z;
        }
        public double Avg()
        {
            return (X + Y + Z)/3;
        }
    }
}
