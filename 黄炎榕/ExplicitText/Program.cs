﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplicitText
{
    class Program
    {
        static void Main(string[] args)
        {
            ComputerMajor major = new ComputerMajor();
            IComputer computer = major; //创建接口的实例
            computer.Id = 6;
            computer.Name = "黄炎榕";
            computer.Sex = "男";
            major.X = 120;
            major.Y = 140;
            major.Z = 278;
            Console.WriteLine("此处为显式接口：");
            Console.WriteLine("学生信息：");
            Console.WriteLine("学号：{0}，姓名：{1}，性别：{2}",computer.Id,computer.Name,computer.Sex);
            Console.WriteLine("成绩信息：");
            Console.WriteLine("总分为：{0}，平均分为：{1}",computer.Total(),computer.Avg());
            Console.ReadKey();
        }
    }
}
