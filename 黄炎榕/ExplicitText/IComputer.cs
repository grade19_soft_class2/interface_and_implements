﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplicitText
{
    interface IComputer //定义接口
    {
        int Id { get; set; }
        string Name { get; set; }
        string Sex { get; set; }
        double Total();
        double Avg();
    }
}
