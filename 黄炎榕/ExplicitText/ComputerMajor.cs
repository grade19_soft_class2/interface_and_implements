﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplicitText
{
    class ComputerMajor : IComputer
    {
        int IComputer.Id { get; set; }  //显式实现接口中的属性
        string IComputer.Name { get; set; }
        string IComputer.Sex { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        double IComputer.Total() //显式实现接口中的方法
        {
            return X + Y + Z;
        }
        double IComputer.Avg()
        {
            return (X + Y + Z)/3;
        }
    }
}
