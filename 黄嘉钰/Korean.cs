﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp9
{
    //隐式实现接口
    class Korean : ILanguage
    {
        public string Vowel { get ; set ; }
        public string Consonant { get ; set ; }
        public string Word { get ; set ; }
        public string Remark { get; set; }
    }
}
