﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp9
{
    interface ILanguage
    {
        string Vowel { get; set; }//元音
        string Consonant { get; set; }//辅音
        string Word { get; set; }
    }
}
