﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp9
{
    //显示实现接口
    class Chinese : ILanguage
    {
        string ILanguage.Vowel { get ; set ; }
        string ILanguage.Consonant { get ; set ; }
        string ILanguage.Word { get ; set ; }
        string Remark { get; set; }

    }
}
