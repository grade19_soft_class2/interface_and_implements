﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            //隐式实现接口
            Korean korean = new Korean();
            string k1 = korean.Vowel = "ㅔ(ei)";
            string k2 = korean.Consonant = "ㅅ(s)";
            string k3 = korean.Word = "세(sei)";
            Console.WriteLine("{0}+{1}={2}",k2,k1,k3);

            string k4 = korean.Vowel = "ㅡ(e)";
            string k5 = korean.Consonant = "ㅂ(b) ㄴ(n)";
            string k6 = korean.Word = "븐(ben)";
            Console.WriteLine("{0}+{1}={2}", k5, k4, k6);

            string k7 = korean.Vowel = "ㅣ(i)";
            string k8 = korean.Consonant = "ㅌ(t) ㄴ(n)";
            string k9 = korean.Word = "틴(tin)";
            Console.WriteLine("{0}+{1}={2}", k7, k8, k9);

            Console.WriteLine("{0}{1}{2}",k3,k6,k9);
            korean.Remark = "中文意思：十七";

            
            //显示实现接口
            ILanguage chinese = new Chinese();
            string c1 = chinese.Vowel = "ǐ";
            string c2 = chinese.Consonant = "n";
            string c3 = chinese.Word = "你";
            Console.WriteLine("{0}+{1}={2}", c2, c1, c3);

            string c4 = chinese.Vowel = "ǎo";
            string c5 = chinese.Consonant = "h";
            string c6 = chinese.Word = "好";
            Console.WriteLine("{0}+{1}={2}", c5, c4, c6);

            Console.WriteLine("{0}{1}",c3,c6);
            korean.Remark = "韩语意思：안녕하세요 ";
             
        }
    }
}
