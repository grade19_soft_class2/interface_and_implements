﻿using System;

namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            //隐式接口
            Pig pig = new Pig();
            pig.Id = 0;
            pig.Name = "猪猪";
            pig.Age = 20;
            pig.Hige=160;
            
            Console.WriteLine("这一个项目做的是隐式接口!!!!");
            Console.WriteLine("编号："+pig.Id);
            Console.WriteLine("名字："+pig.Name);
            Console.WriteLine("年龄："+pig.Age);
            Console.WriteLine("身高："+pig.Hige);
            Console.ReadKey();
        }
    }
}
