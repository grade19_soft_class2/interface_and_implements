﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp10
{
    public interface LOng
    {   
        
        int Money { get; set; }
        string Name { get; set; }
        string Color { get; set; }
        string Cute { get; set; }
        int Num { get; set; }
        double Ave();
        double Sum();
    }
}
