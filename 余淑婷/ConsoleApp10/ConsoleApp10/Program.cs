﻿using System;

namespace ConsoleApp10
{
    class Program
    {
        static void Main(string[] args)
        {
            Elephant elephant = new Elephant();
            LOng ele = elephant;
            ele.Money = 9999999;
            ele.Name = "江";
            ele.Cute = "120分";
            ele.Color = "粉色";
            Console.WriteLine("这是显示的接口。");
            Console.WriteLine("名字："+ele.Name);
            Console.WriteLine("剩余的钱："+ele.Money);
            Console.WriteLine("可爱程度："+ele.Cute);
            Console.WriteLine("颜色："+ele.Color);
            elephant.a = 95;
            elephant.b = 90;
            ele.Ave();
            ele.Sum();
            Console.WriteLine("分数和：{0}  平均数：{1}",ele.Sum(),ele.Ave());
            Console.ReadKey();
        }
    }
}
