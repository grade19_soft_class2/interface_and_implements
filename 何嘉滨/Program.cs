﻿using System;
using System.Reflection.Metadata.Ecma335;
using System.Collections.Generic;
using System.Text;
//接口的定义：接口的定义是指定一组函数成员而不实现成员的引用类型，
//其它类型和接口可以继承接口。定义还是很好理解的，
//但是没有反映特点，接口主要有以下特点：
//通过接口可以实现多重继承，
//C#接口的成员不能有public、protected、internal、private等修饰符。
//原因很简单，接口里面的方法都需要由外面接口实现去实现方法体，那么其修饰符必然是public。
//C#接口中的成员默认是public的，java中是可以加public的
//)接口成员不能有new、static、abstract、override、virtual修饰符
//有一点要注意，当一个接口实现一个接口，这2个接口中有相同的方法时，
//接口中只包含成员的签名，接口没有构造函数，所有不能直接使用new对接口进行实例化。可用new关键字隐藏父接口中的方法。
//接口中只能包含方法、属性、事件和索引的组合。接口一旦被实现，实现类必须实现接口中的所有成员，除非实现类本身是抽象类。
//
namespace interfaceText
{
   public  interface ITextInterface//隐式接口
        //接口开头都带I
    {
        void ITextInterface();//
        //对接口声明
    }
    public interface Ishout
    {
        void Ishout();
    }
    class Text : Ishout
    {
        public void Ishout()
        {
            throw new NotImplementedException();

        }
        
    }
    
}
