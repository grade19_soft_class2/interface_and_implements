﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    interface IAnimal
    {
         public void Run();

         public void Cry();
    }
    class Dog : IAnimal
    {
        public void Run()
        {
            Console.WriteLine("我会奔跑！");
        }
        public void Cry()
        {
            Console.WriteLine("我会哭泣！");
        }
    }
        class Cat : IAnimal
        {
            public void Run()
            {
                Console.WriteLine("我会奔跑！");
            }
            public void Cry()
            {
                Console.WriteLine("我会哭泣！");
            }
        }   
    

}
