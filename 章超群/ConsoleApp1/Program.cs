﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Dog dog = new Dog();
            dog.Cry();
            dog.Run();
            IAnimal animal = new Cat();
            animal.Run();
            animal.Cry ();

            /*接口的显示调用*/
            BigCompute bigCompute = new BigCompute();

            IComputer computer = bigCompute;

            computer.Big ();
            var bAge=computer.Age = 100;
           var cWeight=computer.Weight = 5000.100;

            Console.WriteLine(bAge);

            Console.WriteLine(cWeight);

            /*接口的隐示调用*/


            Compute compute=new Compute();

            var newAge=compute.Age = 50;
            
            var newWeight=compute.Weight = 60.5;

            compute.Big();

            Console.WriteLine(newAge);

            Console.WriteLine(newWeight);
        }
    }
}
