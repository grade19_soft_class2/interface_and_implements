﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    interface IComputer
    {
        public int Age { get; set; }

        public double Weight { get; set; }

        public void Big();

    }

    class BigCompute : IComputer
    {
         int IComputer.Age { get; set; }

         double IComputer.Weight { get; set; }

        void IComputer.Big()
        {
            Console.WriteLine("显示接口使用");
        }


    }
    class Compute : IComputer
    {
        public int Age { get; set; }
        public double Weight { get; set; }

        public void Big()
        {
            Console.WriteLine("隐示接口使用");
        }
    }
}
