﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Mango:IMango
    {
        /*public int Id { get; set; }
        public string Name { get; set; }
        public double English { get; set; }
        public double Programming { get; set; }
        public double Datebase { get; set; }
        public void Array()
        {
            double arr = (English + Programming + Datebase) / 3;
            Console.WriteLine("三科的平均分"+arr );
        }
        public void Sum()
        {
            double sum = English + Programming + Datebase;
            Console.WriteLine("总分"+sum );
        }*/


        
        int IMango.Id { get; set; }
        string IMango.Name { get; set; }
        public double English { get; set; }
        public double Programming { get; set; }
        public double Datebase { get; set; }
        void IMango.Arr()
        {
            double arr = (English + Programming + Datebase) / 3;
            Console.WriteLine("平均分"+arr );
        }
        void IMango.Sum()
        {
            double sum = English + Programming + Datebase;
            Console.WriteLine("总分"+sum );
        }
    }

    interface IMango
    {
        int Id { get; set; }
        string Name { get; set; }
        void Arr();
        void Sum();
    }
}
