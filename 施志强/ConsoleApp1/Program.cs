﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Mango mango = new Mango();
            mango.Id = 1;
            mango.Name = "芒果";
            mango.English = 80;
            mango.Programming = 80;
            mango.Datebase = 80;
            Console.WriteLine("学号"+mango.Id );
            Console.WriteLine("名字"+mango.Name );
            Console.WriteLine("英语成绩{0}  编程成绩{1}  数据库成绩{2}",mango.English,mango.Programming,mango.Datebase);
            mango.Array();
            mango.Sum();*/



            Mango mango = new Mango();
            IMango mango1 = mango;
            mango1.Id = 2;
            mango1.Name = "芒果";
            mango.English = 80;
            mango.Programming = 80;
            mango.Datebase = 80;
            Console.WriteLine("学号" + mango1.Id);
            Console.WriteLine("名字" + mango1.Name);
            Console.WriteLine("英语成绩{0}  编程成绩{1}  数据库成绩{2}", mango.English, mango.Programming, mango.Datebase);
            mango1.Arr();
            mango1.Sum();
        }
    }
}
