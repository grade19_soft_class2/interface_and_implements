﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    interface IJump
    {
        int Age { get; set; }
        string Name { get; set; }
        string Color { get; set; }
        int Height { get; set; }
    }
}
