﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Dog d = new Dog();
            IJump dog = d;
            dog.Age = 2;
            dog.Name = "贝贝";
            dog.Color = "白";
            dog.Height = 1;
            Console.WriteLine("狗狗今年{0}岁，名叫{1}，是{2}色的，可以跳约{3}米高", dog.Age, dog.Name, dog.Color, dog.Height);

        }
    }
}
