﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Dog : IJump
    {
        //显式实现

        int IJump.Age { get; set; }
        string IJump.Name { get; set; }
        string IJump.Color { get; set; }
        int IJump.Height { get; set; }

    }
}
