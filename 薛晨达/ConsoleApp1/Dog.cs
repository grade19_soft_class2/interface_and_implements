﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Dog : IJump
    {
        //隐式实现

        public int Age { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public int Height;

        public void Jump()
        {
            Console.WriteLine("狗狗今年{0}岁，名叫{1}，是{2}色的，可以跳约{3}米高", Age, Name, Color, Height);
        }
    }
}
