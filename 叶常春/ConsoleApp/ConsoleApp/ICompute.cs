﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    interface Interface
    {
        string name { get; set; }
        int Studentnumber { get; set;  }
        string dialect { get; set; }
        string from { get; set; }

        void comefrom();
        void speak();
    }
}
