﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class FuJianren : Interface
    {
        public string name { get; set; }
        public int Studentnumber { get; set; }
        public string dialect { get; set; }
        public string from { get; set;  }
        public void comefrom()
        {
            Console.WriteLine("来自："+from);
        }
        public void speak()
        {
            Console.WriteLine("说话方式是说："+dialect);
        }
    }
    class Beijing : Interface
    {
        string Interface.name { get; set; }
        int Interface.Studentnumber { get; set; }
        public string dialect { get; set; }
        public string from { get; set; }
        void Interface.comefrom()
        {
            Console.WriteLine("来自：" + from);
        }
        void Interface.speak()
        {
            Console.WriteLine("说话方式是说：" + dialect);
        }
    }

}
