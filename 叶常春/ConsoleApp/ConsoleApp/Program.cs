﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            FuJianren fuJianren = new FuJianren();
            Beijing beijing = new Beijing();
            Interface face = beijing;
            fuJianren.name = "浪子叶三少";
            fuJianren.from = "江西";
            fuJianren.Studentnumber = 1922010110;
            fuJianren.dialect = "客家话";
            Console.WriteLine("学生名字：{0}，学生学号：{1}",fuJianren.name,fuJianren.Studentnumber);
            fuJianren.comefrom();
            fuJianren.speak();
            face.name = "徐汉权傻逼";
            face.from = "四川";
            face.Studentnumber = 1922010111;
            face.dialect = "脑瘫话";
            Console.WriteLine("学生名字：{0}，学生学号：{1}", face.name, face.Studentnumber);
            face.comefrom();
            face.speak();
        }
    }
}
