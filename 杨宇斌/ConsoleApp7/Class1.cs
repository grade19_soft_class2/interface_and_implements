﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
   public class Class1 : IAnimal
    {
        int IAnimal.Id { get; set; }  //显示实现接口中的属性
        string IAnimal.Name { get; set; }

        void IAnimal.Eat()  //显示实现接口中的方法
        {
            Console.WriteLine("动物去吃零食"); 
        }

    }
    class Cat : Class1
    {
       public void Eat()
        {
            Console.WriteLine("猫吃水果");
        }
    
    }
}
