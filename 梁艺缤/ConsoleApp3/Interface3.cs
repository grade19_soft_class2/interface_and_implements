﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    //显示接口的实现
    public interface IPerson
    {
        string Name { get; set; }

        void Show(string name);
    }
    
    public interface IStudent
    {
        string Id { get; set; }
        void Show(string id);
    }
   
    public class Student: IPerson,IStudent
    {
        public string Name{  get;set; }
        
        public string Id { get;set; }
        void IPerson.Show(string name)
        {
            Console.WriteLine("姓名：{0}",name );
        }
        void IStudent.Show(string id)
        {
            Console.WriteLine("学号：{0}", id);
        }       
    }    
}
