﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class ComputerMajor : ICompute
    {
        public int Id { get; set; }     //隐式的实现接口中的属性
        public string Name { get; set; }    //隐式实现接口中的属性
        public double English { get; set; }
        public double Programming { get; set; }
        public double Database { get; set; }
        public void Avg()       //隐式实现接口中的方法
        {
            double avg = (English + Programming + Database) / 3;
            Console.WriteLine("平均分：" + avg);
        }
        public void Total()
        {
            double sum = English + Programming + Database;
            Console.WriteLine("总分为：" + sum);
        }
    }
}
