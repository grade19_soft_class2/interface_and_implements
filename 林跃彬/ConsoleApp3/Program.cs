﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            ComputerMajor computerMajor = new ComputerMajor();
            computerMajor.Id = 1;
            computerMajor.Name = "林跃彬";
            computerMajor.English = 100;
            computerMajor.Programming = 100;
            computerMajor.Database = 99;
            Console.WriteLine("学号：" + computerMajor.Id);
            Console.WriteLine("姓名：" + computerMajor.Name);
            Console.WriteLine("成绩信息如下：");
            computerMajor.Total();
            computerMajor.Avg();



            ITest test1 = new Test1();  //创建接口的实例test1指向实现类Test1的对象
            test1.methodA();
            ITest test2 = new Test2();  //创建接口的实例test2指向实现类Test2的对象
            test2.methodA();
        }
    }
}
