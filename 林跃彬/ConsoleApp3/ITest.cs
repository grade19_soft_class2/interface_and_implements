﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    interface ITest
    {
        void methodA();
    }
    class Test1 : ITest
    {
        public void methodA()
        {
            Console.WriteLine("Test1 类中的 methodA 方法");
        }
    }
    class Test2 : ITest
    {
        public void methodA()
        {
            Console.WriteLine("Test2 类中的 methodA 方法");
        }
    }
}
