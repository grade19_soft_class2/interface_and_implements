﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShowHidden
{
    class Program
    {
        static void Main(string[] args)
        {
            // 隐式实现接口
            ComputerMajorHide computerMajorHide = new ComputerMajorHide();
            computerMajorHide.ID = 1;
            computerMajorHide.Name = "墨染";
            computerMajorHide.Programming = 91.1;
            computerMajorHide.English = 90.1;
            computerMajorHide.Database = 91.9;
            Console.WriteLine("学号:" + computerMajorHide.ID);
            Console.WriteLine("姓名:" + computerMajorHide.Name);
            Console.WriteLine("成绩如下");
            computerMajorHide.Score();
            computerMajorHide.Avg();
            computerMajorHide.Total();
            Console.WriteLine("*-*-*-*-*-*- E N D -*-*-*-*-*-*");
            //显示实现接口
            ComputerMajorExplicit computerMajorExplicit = new ComputerMajorExplicit();
            IComeputer comeputer = computerMajorExplicit;
            comeputer.ID = 2;
            comeputer.Name = "山水";
            computerMajorExplicit.English = 88;
            computerMajorExplicit.Programming = 83;
            computerMajorExplicit.Database = 75;
            Console.WriteLine("学号:" + comeputer.ID);
            Console.WriteLine("姓名:" + comeputer.Name);
            Console.WriteLine("成绩信息如下");
            computerMajorExplicit.Score();
            computerMajorExplicit.Total();
            computerMajorExplicit.Avg();
        }
    }
}
