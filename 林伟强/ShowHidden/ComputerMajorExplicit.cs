﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShowHidden
{
    interface IComeputer
    {
        int ID { get; set; }
        string Name { get; set; }
        void Avg();
        void Total();
    }

    class ComputerMajorExplicit : IComeputer
    {
        public double English { get; set; }
        public double Programming { get; set; }
        public double Database { get; set; }
        int IComeputer.ID { get; set; } //显示实现接口中的属性
        string IComeputer.Name { get; set; }
        public void Score() 
        {
            Console.WriteLine("English:" + English);
            Console.WriteLine("Programming:" + Programming);
            Console.WriteLine("Database:" + Database);
        }
        public void Avg() //显示实现接口中的方法
        {
            double avg = (English + Programming + Database) / 3;
            double b = Math.Round(avg, 2);
            Console.WriteLine("平均分：" + b);
        }
        public void Total() //显示实现接口中的方法
        {
            double sum = English + Programming + Database;
            Console.WriteLine("总分为：" + sum);
        }
    }
}
